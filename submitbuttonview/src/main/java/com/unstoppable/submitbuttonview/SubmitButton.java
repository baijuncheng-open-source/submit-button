package com.unstoppable.submitbuttonview;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.render.PathMeasure;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.multimodalinput.event.TouchEvent;
import static java.lang.Math.abs;
public class SubmitButton extends ComponentContainer implements Component.EstimateSizeListener, Component.DrawTask, Component.BindStateChangedListener {
    private static final int STATE_NONE = 0;
    private static final int STATE_SUBMIT = 1;
    private static final int STATE_LOADING = 2;
    private static final int STATE_RESULT = 3;
    private Button button;
    private int viewState = STATE_NONE;
    private int mWidth;
    private int mHeight;
    private int MAX_WIDTH;
    private int MAX_HEIGHT;
    private int x, y;
    private String buttonText = "";
    private Color buttonColor;
    private Color succeedColor;
    private Color failedColor;
    private int textSize;
    private int textWidth;
    private int textHeight;
    private Paint bgPaint, loadingPaint, resultPaint, textPaint;
    private Path buttonPath;
    private Path loadPath;
    private Path dst;
    private PathMeasure pathMeasure;
    private Path resultPath;
    private RectFloat circleLeft, circleMid, circleRight;
    private float loadValue;
    private AnimatorValue submitAnim = new AnimatorValue();
    private AnimatorValue loadingAnim = new AnimatorValue();
    private AnimatorValue resultAnim = new AnimatorValue();
    private boolean isDoResult;
    private boolean isSucceed;
    private static final int STYLE_LOADING = 0;
    private static final int STYLE_PROGRESS = 1;
    private int progressStyle = STYLE_LOADING;
    private float currentProgress;
    private OnResultEndListener listener;
    public SubmitButton(Context context) {
        super(context,null);
    }
    public SubmitButton(Context context, AttrSet attrSet) {
        super(context,attrSet);
        button = new Button(context,attrSet);
        if (attrSet.getAttr("buttonText").isPresent()) {
            buttonText = attrSet.getAttr("buttonText").get().getStringValue();
        } else {
            buttonText = null;
        }
        if (attrSet.getAttr("buttonTextSize").isPresent()) {
            textSize = attrSet.getAttr("buttonTextSize").get().getDimensionValue();
        } else {
            textSize = 0;
        }
        if (attrSet.getAttr("progressStyle_progress").isPresent()) {
            progressStyle = STYLE_PROGRESS;
        }
        buttonColor = new Color(Color.rgb(25, 204,149));
        succeedColor = new Color(Color.rgb(25, 204,149));
        failedColor = new Color(Color.rgb(252, 142, 52));
        init();
        this.setEstimateSizeListener(this::onEstimateSize);
        this.setTouchEventListener(new TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                switch (touchEvent.getAction()) {
                    case TouchEvent.PRIMARY_POINT_DOWN:
                        if (viewState == STATE_NONE) {
                            startSubmitAnim();
                        } else {
                            return true;
                        }
                }
                return false;
            }
        });
        addDrawTask(this::onDraw);
    }
    public SubmitButton(Context context, AttrSet attrSet, String styleName) {
        super(context,attrSet,styleName);
    }
    private void init() {
        bgPaint = new Paint();
        bgPaint.setColor(buttonColor);
        bgPaint.setStyle(Paint.Style.STROKE_STYLE);
        bgPaint.setStrokeWidth(5);
        bgPaint.setAntiAlias(true);
        loadingPaint = new Paint();
        loadingPaint.setColor(buttonColor);
        loadingPaint.setStyle(Paint.Style.STROKE_STYLE);
        loadingPaint.setStrokeWidth(9);
        loadingPaint.setAntiAlias(true);
        resultPaint = new Paint();
        resultPaint.setColor(Color.WHITE);
        resultPaint.setStyle(Paint.Style.STROKE_STYLE);
        resultPaint.setStrokeWidth(9);
        resultPaint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
        resultPaint.setAntiAlias(true);
        textPaint = new Paint();
        textPaint.setColor(buttonColor);
        textPaint.setStrokeWidth(textSize / 6);
        textPaint.setTextSize(textSize);
        textPaint.setAntiAlias(true);
        textWidth = getTextWidth(textPaint,buttonText);
        textHeight = getTextHeight(textPaint,buttonText);
        buttonPath = new Path();
        loadPath = new Path();
        resultPath = new Path();
        dst = new Path();
        circleMid = new RectFloat();
        circleLeft = new RectFloat();
        circleRight = new RectFloat();
        pathMeasure = new PathMeasure(null,false);
    }
    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        int widthMode = EstimateSpec.getMode(widthEstimateConfig);
        int widthSize = EstimateSpec.getSize(widthEstimateConfig);
        int heightMode = EstimateSpec.getMode(heightEstimateConfig);
        int heightSize = EstimateSpec.getSize(heightEstimateConfig);

        if (widthMode == EstimateSpec.NOT_EXCEED) {
            widthSize = textWidth + 100;
        }
        if (heightMode == EstimateSpec.NOT_EXCEED) {
            heightSize = (int) (textHeight * 2.5);
        }
        if (heightSize > widthSize) {
            heightSize = (int) (widthSize * 0.25);
        }
        mWidth = widthSize - 10;
        mHeight = heightSize - 10;
        x = (int) (widthSize * 0.5);
        y = (int) (heightSize * 0.5);
        MAX_WIDTH = mWidth;
        MAX_HEIGHT = mHeight;
        setEstimatedSize(widthSize,heightSize);
        return true;
    }
    @Override
    public void onDraw(Component component, Canvas canvas) {
        canvas.translate(x, y);
        drawButton(canvas);
        if (viewState == STATE_NONE || viewState == STATE_SUBMIT && mWidth > textWidth) {
            drawButtonText(canvas);
        }
        if (viewState == STATE_LOADING) {
            drawLoading(canvas);
        }
        if (viewState == STATE_RESULT) {
            drawResult(canvas, isSucceed);
        }
    }
    private void drawButton(Canvas canvas) {
        buttonPath.reset();
        circleLeft.modify(-mWidth / 2, -mHeight / 2, -mWidth / 2 + mHeight, mHeight / 2);
        buttonPath.arcTo(circleLeft, 90, 180);
        buttonPath.lineTo(mWidth / 2 - mHeight / 2, -mHeight / 2);
        circleRight.modify(mWidth / 2 - mHeight, -mHeight / 2, mWidth / 2, mHeight / 2);
        buttonPath.arcTo(circleRight, 270, 180);
        buttonPath.lineTo(-mWidth / 2 + mHeight / 2, mHeight / 2);
        canvas.drawPath(buttonPath, bgPaint);
    }
    private void drawButtonText(Canvas canvas) {
        canvas.drawText(textPaint, buttonText, -textWidth / 2, getTextBaseLineOffset());
    }
    private void drawLoading(Canvas canvas) {
        dst.reset();
        circleMid.modify(-MAX_HEIGHT / 2, -MAX_HEIGHT / 2, MAX_HEIGHT / 2, MAX_HEIGHT / 2);
        loadPath.addArc(circleMid, 270, 359.999f);
        pathMeasure.setPath(loadPath,true);
        float startD = 0f, stopD;
        if (progressStyle == STYLE_LOADING) {
            startD = pathMeasure.getLength() * loadValue;
            stopD = startD + pathMeasure.getLength() / 2 * loadValue;
        } else {

            stopD = pathMeasure.getLength() * currentProgress;
        }
        pathMeasure.getSegment(startD, stopD, dst, true);
        canvas.drawPath(dst, loadingPaint);
    }
    private void drawResult(Canvas canvas, boolean isSucceed) {
        if (isSucceed) {
            resultPath.moveTo(-mHeight / 6, 0);
            resultPath.lineTo(0, (float) (-mHeight / 6 + (1 + Math.sqrt(5)) * mHeight / 12));
            resultPath.lineTo(mHeight / 6, -mHeight / 6);
        } else {
            resultPath.moveTo(-mHeight / 6, mHeight / 6);
            resultPath.lineTo(mHeight / 6, -mHeight / 6);
            resultPath.moveTo(-mHeight / 6, -mHeight / 6);
            resultPath.lineTo(mHeight / 6, mHeight / 6);
        }
        canvas.drawPath(resultPath, resultPaint);
    }
    private void startSubmitAnim() {
        viewState = STATE_SUBMIT;
        submitAnim.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                mWidth = MAX_HEIGHT + (int)((MAX_WIDTH - MAX_HEIGHT) * (1 - v));
                if (mWidth == mHeight) {
                    bgPaint.setColor(new Color(Color.rgb(221,221,221)));
                }
                invalidate();
            }
        });
        submitAnim.setDuration(300);
        submitAnim.setCurveType(Animator.CurveType.ACCELERATE);
        submitAnim.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
            }
            @Override
            public void onStop(Animator animator) {
            }
            @Override
            public void onCancel(Animator animator) {
            }
            @Override
            public void onEnd(Animator animator) {
                if (isDoResult) {
                    startResultAnim();
                } else {
                    startLoadingAnim();
                }
            }
            @Override
            public void onPause(Animator animator) {
            }
            @Override
            public void onResume(Animator animator) {
            }
        });
        submitAnim.start();
    }
    private void startLoadingAnim() {
        viewState = STATE_LOADING;
        if (progressStyle == STYLE_PROGRESS) {
            return;
        }
        loadingAnim.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                loadValue = v;
                invalidate();
            }
        });
        loadingAnim.setDuration(2000);
        loadingAnim.setLoopedCount(AnimatorValue.INFINITE);
        loadingAnim.start();
    }
    private void startResultAnim() {
        viewState = STATE_RESULT;
        if (loadingAnim != null) {
            loadingAnim.cancel();
        }
        resultAnim.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                mWidth = MAX_HEIGHT + (int)((abs(MAX_WIDTH - MAX_HEIGHT)) * (v - 0.02777778));
                resultPaint.setAlpha(((mWidth - mHeight) * 255) / (MAX_WIDTH - MAX_HEIGHT));
                if (mWidth == mHeight) {
                    if (isSucceed) {
                        bgPaint.setColor(succeedColor);
                    } else {
                        bgPaint.setColor(failedColor);
                    }
                    bgPaint.setStyle(Paint.Style.FILLANDSTROKE_STYLE);
                }
                invalidate();
            }
        });
        resultAnim.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
            }
            @Override
            public void onStop(Animator animator) {
            }
            @Override
            public void onCancel(Animator animator) {
            }
            @Override
            public void onEnd(Animator animator) {
                if (listener == null) {
                    return;
                }
                EventHandler eventHandler = new EventHandler(EventRunner.current()) {
                    @Override
                    protected void processEvent(InnerEvent event) {
                        super.processEvent(event);
                    }
                };
                new Runnable() {
                    @Override
                    public void run() {
                        listener.onResultEnd();
                        eventHandler.postTask(this::run,500);
                    }
                };
            }
            @Override
            public void onPause(Animator animator) {
            }
            @Override
            public void onResume(Animator animator) {
            }
        });
        resultAnim.setDuration(300);
        resultAnim.setCurveType(Animator.CurveType.ACCELERATE);
        resultAnim.start();
    }
    @Override
    public void onComponentBoundToWindow(Component component) {
    }
    @Override
    public void onComponentUnboundFromWindow(Component component) {
        if (submitAnim != null) {
            submitAnim.cancel();
        }
        if (loadingAnim != null) {
            loadingAnim.cancel();
        }
        if (resultAnim != null) {
            resultAnim.cancel();
        }
    }
    public void doResult(boolean isSucceed) {
        if (viewState == STATE_NONE || viewState == STATE_RESULT || isDoResult) {
            return;
        }
        isDoResult = true;
        this.isSucceed = isSucceed;
        if (viewState == STATE_LOADING) {
            startResultAnim();
        }
    }
    public void reset() {
        if (submitAnim != null) {
            submitAnim.cancel();
        }
        if (loadingAnim != null) {
            loadingAnim.cancel();
        }
        if (resultAnim != null) {
            resultAnim.cancel();
        }
        viewState = STATE_NONE;
        mWidth = MAX_WIDTH;
        mHeight = MAX_HEIGHT;
        isSucceed = false;
        isDoResult = false;
        currentProgress = 0;
        init();
        invalidate();
    }
    public void setProgress(int progress) {
        if (progress < 0 || progress > 100) {
            return;
        }
        currentProgress = (float) (progress * 0.01);
        if (progressStyle == STYLE_PROGRESS && viewState == STATE_LOADING) {
            invalidate();
        }
    }
    public interface OnResultEndListener {
        void onResultEnd();
    }
    public void setOnResultEndListener(OnResultEndListener listener) {
        this.listener = listener;
    }

    private float getTextBaseLineOffset() {
        Paint.FontMetrics fm = textPaint.getFontMetrics();
        return -(fm.bottom + fm.top) / 2;
    }
    private int getTextWidth(Paint paint, String str) {
        int mRet = 0;
        if (str != null && str.length() > 0) {
            int len = str.length();
            float[] widths = new float[len];
            paint.getAdvanceWidths(str,widths);
            for (int j = 0; j < len; j++) {
                mRet += (int) Math.ceil(widths[j]);
            }
        }
        return mRet;
    }
    private int getTextHeight(Paint paint, String str) {
        return paint.getTextBounds(str).getHeight();
    }
}
