## SubmitButton


>It's a submit button with a fun animation application.

# demo effect
![](/screens/submitbutton_failed.gif)
![](/screens/submitbutton_progress.gif)
![](/screens/submitbutton_succeed.gif)

## Getting Started

##### 1.Specify SubmitButton as a dependency in your build.gradle file;

	dependencies {
	    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	    testCompile 'junit:junit:4.12'
	    compile project(path: ':submitbuttonview')
	}

##### 2.Remote dependency
```
dependencies {
        implementation ‘com.gitee.baijuncheng-open-source:SubmitButton:1.0.0’
}
```

##### 2.Add SubmitButton to the layout file;

	<com.unstoppable.submitbuttonview.SubmitButton
	        ohos:id="$+id:sbtn_progress"
	        ohos:height="50vp"
	        ohos:width="200vp"
	        ohos:center_in_parent="true"
	        ohos:top_margin="30vp"
	        ohos:visibility="invisible"
	        subbtn:buttonText="Submit"
	        subbtn:buttonTextSize="25fp"
	        subbtn:progressStyle_progress="1"
	        />

##### 3.Attribute

| name          | format   | description                                           |default  |
|:--------------|:-----    |:------------------------------------------------------|:--------|
|buttonColor    |color     |set the button theme color                             |#19CC95  |
|buttonText     |String    |set the button text                                    |null     |
|buttonTextSize |dimension |set the button text size                               |15sp     |
|succeedColor   |color     |set the button color after the submission is successful|#19CC95  |
|failedColor    |color     |set the button color after the submission fails        |#FC8E34  |
|progressStyle  |enum      |set the button progress style (Optional:loading or progress) |loading|

##### 4.Method

	 /**
	 * Pass the results to show different feedback results
	 *
	 * @param boolean isSucceed 
	 */
	mSubmitView.doResult(boolean isSucceed);
	
	 /**
	 * Reset SubmitButton 
	 */
	mSubmitView.reset();
	
	/**
	 * set progress(This method is valid only if progressStyle is set to progress)
	 *
	 * @param progress (0-100)
	 */
	mSubmitView.setProgress();
	
	/**
	 * set the animation end callback interface
	 *
	 * @param listener
	 */
	mSubmitView.setOnResultEndListener(OnResultEndListener listener)


## Change Log
### [v1.0.0]) (2021-07-12)
- 新增SubmitButton库

- 修改稳定性问题，运行异常问题


## License

	Copyright 2017 Unstoppable
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	   http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
