package com.unstoppable.submitbutton;

import com.unstoppable.submitbuttonview.SubmitButton;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.Button;
import ohos.agp.components.AbsButton;
import ohos.agp.components.ComponentState;
import ohos.agp.components.Switch;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.window.dialog.ToastDialog;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
public class MainAbility extends Ability implements Component.ClickedListener {
    private SubmitButton sBtnLoading, sBtnProgress;
    private Button btnSucceed, btnFailed, btnReset;
    private Switch mSwitch;
    private MyTask task;
    private StateElement trackElementInit(ShapeElement on, ShapeElement off) {
        StateElement trackElement = new StateElement();
        trackElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, on);
        trackElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, off);
        return trackElement;
    }
    private StateElement thumbElementInit(ShapeElement on, ShapeElement off) {
        StateElement thumbElement = new StateElement();
        thumbElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, on);
        thumbElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, off);
        return thumbElement;
    }
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);
        sBtnLoading = (SubmitButton) findComponentById(ResourceTable.Id_sbtn_loading);
        sBtnProgress = (SubmitButton) findComponentById(ResourceTable.Id_sbtn_progress);
        mSwitch = (Switch) findComponentById(ResourceTable.Id_switch1);
        btnFailed = (Button) findComponentById(ResourceTable.Id_btn_failed);
        btnSucceed = (Button) findComponentById(ResourceTable.Id_btn_succeed);
        btnReset = (Button) findComponentById(ResourceTable.Id_btn_reset);
        sBtnLoading.setClickedListener(this);
        sBtnProgress.setClickedListener(this);
        btnSucceed.setClickedListener(this);
        btnFailed.setClickedListener(this);
        btnReset.setClickedListener(this);
        sBtnLoading.setOnResultEndListener(new SubmitButton.OnResultEndListener() {
            @Override
            public void onResultEnd() {
                new ToastDialog(getContext()).setText("ResultEnd").show();
            }
        });
        mSwitch.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean isChecked) {
                ShapeElement elementThumbOn = new ShapeElement();
                elementThumbOn.setShape(ShapeElement.OVAL);
                elementThumbOn.setRgbColor(new RgbColor(225,225,225));
                elementThumbOn.setCornerRadius(50);
                ShapeElement elementThumbOff = new ShapeElement();
                elementThumbOff.setShape(ShapeElement.OVAL);
                elementThumbOff.setRgbColor(new RgbColor(225,225,225));
                elementThumbOff.setCornerRadius(50);
                ShapeElement elementTrackOn = new ShapeElement();
                elementTrackOn.setShape(ShapeElement.RECTANGLE);
                elementTrackOn.setRgbColor(new RgbColor(255,20,147));
                elementTrackOn.setCornerRadius(50);
                ShapeElement elementTrackOff = new ShapeElement();
                elementTrackOff.setShape(ShapeElement.RECTANGLE);
                elementTrackOff.setRgbColor(new RgbColor(169,169,169));
                elementTrackOff.setCornerRadius(50);
                mSwitch.setTrackElement(trackElementInit(elementTrackOn, elementTrackOff));
                mSwitch.setThumbElement(thumbElementInit(elementThumbOn, elementThumbOff));
                if (isChecked) {
                    sBtnLoading.setVisibility(Component.HIDE);
                    sBtnProgress.setVisibility(Component.VISIBLE);
                    sBtnLoading.reset();
                } else {
                    sBtnLoading.setVisibility(Component.VISIBLE);
                    sBtnProgress.setVisibility(Component.HIDE);
                    sBtnProgress.reset();
                    //if (task != null && !task.currentThread().isInterrupted()) {
                    if (task != null) {
                        task.interrupt();
                        sBtnProgress.reset();
                    }
                }
            }
        });
    }
    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_sbtn_loading:
                new ToastDialog(getContext()).setText("SubmitButton be clicked").show();
                break;
            case ResourceTable.Id_sbtn_progress:
                task = new MyTask();
                postTask(task,1);
                task.start();
                break;
            case ResourceTable.Id_btn_succeed:
                if (mSwitch.isChecked()) {
                    if (task != null) {
                        task.interrupt();
                    }
                    sBtnProgress.doResult(true);
                } else {
                    sBtnLoading.doResult(true);
                }
                break;
            case ResourceTable.Id_btn_failed:
                if (mSwitch.isChecked()) {
                    task.interrupt();
                    sBtnProgress.doResult(false);
                } else {
                    sBtnLoading.doResult(false);
                }
                break;
            case ResourceTable.Id_btn_reset:
                if (mSwitch.isChecked()) {
                    task.interrupt();
                    sBtnProgress.reset();
                } else {
                    sBtnLoading.reset();
                }
                break;
        }
    }
    public EventHandler UIHandler = new EventHandler(EventRunner.getMainEventRunner()) {
        @Override
        protected void processEvent(InnerEvent innerEvent) {
            super.processEvent(innerEvent);
            switch (innerEvent.eventId) {
                case 1001:
                    sBtnProgress.setProgress((int)innerEvent.object);
                    break;
                case 1002:
                    if (innerEvent.object == null) {
                        sBtnProgress.reset();
                    } else {
                        if (innerEvent.object.equals(true)) {
                            sBtnProgress.doResult(true);
                        } else {
                            sBtnProgress.doResult(false);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    };
    private class MyTask extends Thread {
        @Override
        public void run() {
            doInBackground();
        }
        protected void doInBackground() {
            int i = 0;
            while (i <= 100) {
                if (Thread.currentThread().getState() == Thread.State.TERMINATED) {
                    return;
                }
                try {
                    Thread.sleep(30);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                i++;
                onProgressUpdate(i);
            }
            onPostExecute(true);
        }
        protected void onProgressUpdate(Integer i) {
            InnerEvent innerEvent = InnerEvent.get(1001);
            innerEvent.object = i;
            UIHandler.sendEvent(innerEvent);
        }
    }
    public void onPostExecute(Boolean bool) {
        InnerEvent innerEvent = InnerEvent.get(1002);
        if (bool == null) {
            innerEvent.object = null;
        }
        innerEvent.object = bool;
        UIHandler.sendEvent(innerEvent);
    }
}